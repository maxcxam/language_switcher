# README #

# How it's work #

This module add language switcher to header and switch language via _POST request instead _GET request and remove 
language prefix from url and from any links (en/ de/ ru/ etc.)

# How to install #

Copy this dir to root_prestashop/modules 

# or  
cd /root_prestashop/modules

git clone https://maxcxam@bitbucket.org/maxcxam/language_switcher.git

Change the owner ( chown -R www-data:www-data bambini_language/ ) to the user:group apache2

Change permission to 0755 for DIR  and 0644 for FILE 

After that install module from adminCP (for ex. http://prestashop/admin/index.php?controller=AdminModules&token=bla_bla_bla)

# That's it all ;-) #

# Include to header #

For include this switcher  paste {hook h='languageSelection'} in header.tpl

(/var/www/prestashop/themes/YOUR_THEME/header.tpl)