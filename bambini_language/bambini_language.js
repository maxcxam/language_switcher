function setLanguage(id_lang)
    {
        $.ajax({
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            url: baseDir + 'index.php' + '?rand=' + new Date().getTime(),
            data: 'id_lang='+ parseInt(id_lang),
            success: function(msg)
            {
                    location.reload(true);
            }
        });
    }