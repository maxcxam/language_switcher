<?php
if (!defined('_PS_VERSION_')) {
  exit;
}
class Bambini_language extends Module
    {
         public function __construct()
      {
          $this->name = 'bambini_language';
          $this->tab = 'front_office_features';
          $this->version = '0.0.1';
          $this->author = 'Maxcxam for BambiniFashion';
          $this->need_instance = 0;

          $this->bootstrap = true;
          parent::__construct();

          $this->displayName = $this->l('Language selector');
          $this->description = $this->l('Language selector for customers with override Link function{disable en/ de/ ru/ etc. from URL}');
      }

      public function install()
      {

          if (!parent::install()
              || !$this->registerHook('header')
              || !$this->registerHook('languageSelection'))
              return false;
      }

      public function uninstall()
      {
          return parent::uninstall();
      }

      public function hookHeader()
      {
          $this->context->controller->addJS(($this->_path).'bambini_language.js');
      }
      public function hookLanguageSelection($params)
      {
        return $this->display(__FILE__, 'localization_selection.tpl');
      }
    }
    