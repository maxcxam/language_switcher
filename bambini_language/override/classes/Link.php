<?php
/** 
*   Remove /en/ /ru/ etc. from url
**/
class Link extends LinkCore
{
protected function getLangLink($id_lang = null, Context $context = null, $id_shop = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if ((!$this->allow && in_array($id_shop, array($context->shop->id,  null))) || !Language::isMultiLanguageActivated($id_shop) || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop)) {
            return '';
        }

        if (!$id_lang) {
            $id_lang = $context->language->id;
        }

         if(is_string(Language::getIsoById((int)$id_lang)) == true) {
            return '';
            }
                
        return Language::getIsoById($id_lang).'/';
    }
}
        