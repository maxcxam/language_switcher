{if count($languages) > 1}
	<div class="input-field">
		<i class="icons8-language" style="margin-right:5px;"></i>
		<select onchange="setLanguage(this.value);">
			{foreach from=$languages key=k item=language name="languages"}
				{if $language.iso_code == $lang_iso}
					<option value="{$language.id_lang}">{$language.name|regex_replace:"/\s\(.*\)$/":""}</option>
				{/if}
			{/foreach}
	            {foreach from=$languages key=k item=language name="languages"}
					
					{if $language.iso_code != $lang_iso}
						{assign var=indice_lang value=$language.id_lang}
						{if isset($lang_rewrite_urls.$indice_lang)}
								
							
								<option value="{$language.id_lang}"><span>{$language.name|regex_replace:"/\s\(.*\)$/":""}</span></option>
						{else}
							
								
							
								<option value="{$language.id_lang}"><span>{$language.name|regex_replace:"/\s\(.*\)$/":""}</span></option>
						{/if}
					{/if}
							
					
					
				{/foreach}
	    </select>
	</div>
	{/if}